<?php
    //DBとの連携
    $link = mysqli_connect("localhost","root","root","mentalitymemo");
    //サーバー名(ホスト)、データベースユーザ名、パスワード,データベース名
    if(mysqli_connect_error()){
        die("DBへの接続に失敗しました");
    }

    $id = $_POST['id'];

    $query = "SELECT * FROM `posting` WHERE id = '".mysqli_real_escape_string($link,$id)."'";
    $result = mysqli_query($link,$query);
    $row = mysqli_fetch_array($result);
    
    $title = $row[1];
    $memo = $row[3];
    $title1 = "";
    $memo1 ="";

    if(array_key_exists('title1',$_POST) AND array_key_exists('memo1',$_POST)){
        if($_POST['title1'] == null){
            $title1 = "タイトルいれてね";
        } elseif ($_POST['memo1'] == null){
            $memo1 = "本文、内容を書いてね";
        } elseif ($_POST['newCategory'] == null) {
            $query = "UPDATE `posting` SET title = '".mysqli_real_escape_string($link,$_POST['title1'])."' , category = '".mysqli_real_escape_string($link,$_POST['category'])."' , memo = '".mysqli_real_escape_string($link,$_POST['memo1'])."' WHERE id = '".mysqli_real_escape_string($link,$_POST['id1'])."'";
            if(mysqli_query($link,$query)){
                header("LOCATION:mentalPage.php");
            }else{
                $title1 = "アップデートに失敗";
            }
        } else {
            $query = "UPDATE `posting` SET title = '".mysqli_real_escape_string($link,$_POST['title1'])."' , category = '".mysqli_real_escape_string($link,$_POST['newCategory'])."' , memo = '".mysqli_real_escape_string($link,$_POST['memo1'])."' WHERE id = '".mysqli_real_escape_string($link,$_POST['id1'])."'";
            if(mysqli_query($link,$query)){
                header("LOCATION:mentalPage.php");
            }else{
                $title1 = "アップデートに失敗";
            }
            
        }
    }


    function htmlsp($h){
        return htmlspecialchars($h,ENT_QUOTES,"UTF-8");
    }    

?>



<head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

        <title>メモ書き換え</title>
</head>


<body>
         <div class="container">
             <h1>編集画面</h1>
             <a href="mentalPage.php">表示画面</a>
             <p>
                 <?php echo htmlsp($title1.$memo1); ?>            
             </p>
                <form method="post">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">タイトル</label>
                        <input type="text" class="form-control" name="title1" placeholder="タイトル" value="<?php if($_POST['title1'] == null){echo htmlsp($title);} else {echo htmlsp($_POST['title1']);} ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">カテゴリー</label>
                        <?php include("selectCategory.php") ?>
                    </div>
                    <div class="form-goup">
                        <input type="text" class="form-control"  name="newCategory" placeholder="新しくカテゴリーを作成する">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">メモ、記録</label>
                        <textarea class="form-control" rows="3" placeholder="メモ" name="memo1"><?php if($_POST['memo1'] == null){echo htmlsp($memo);} else {echo htmlsp($_POST['memo1']);} ?></textarea>
                    </div>
                    <br>
                    <input type="hidden" name="id1" value="<?php echo htmlsp($row[0]); ?>">
                    <button type="submit" class="btn btn-primary">アップデート</button>
                </form>
        </div>
    
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script> 
</body>

