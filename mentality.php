<?php
    //DBとの連携
    $link = mysqli_connect("localhost","root","root","mentalitymemo");
    //サーバー名(ホスト)、データベースユーザ名、パスワード,データベース名
    if(mysqli_connect_error()){
        die("DBへの接続に失敗しました");
    }

    $title = "";
    $memo = "";
    $move = "";
    
    if(array_key_exists('title',$_POST) AND array_key_exists('memo',$_POST)){
        if($_POST['title'] == null){
            $title = "タイトルを入力してください";
        } elseif ($_POST['memo'] == null){
            $memo = "本文を入力してください";
        } elseif ($_POST['newCategory'] == null) {
            $query = "INSERT INTO `posting` (`title`, `category`, `memo`) VALUES ('".mysqli_real_escape_string($link,$_POST['title'])."','".mysqli_real_escape_string($link,$_POST['category'])."','".mysqli_real_escape_string($link,$_POST['memo'])."')";
            if(mysqli_query($link,$query)){
                $move = "書き込み成功";
                header("LOCATION:mentalPage.php");
            }else{
                $move = "書き込み失敗";
            }
        } else {
            $query = "INSERT INTO `posting` (`title`, `category`, `memo`) VALUES ('".mysqli_real_escape_string($link,$_POST['title'])."','".mysqli_real_escape_string($link,$_POST['newCategory'])."','".mysqli_real_escape_string($link,$_POST['memo'])."')";
            if(mysqli_query($link,$query)){
                $move = "書き込み成功";
                header("LOCATION:mentalPage.php");
            }else{
                $move = "書き込み失敗";
            }
            
        }
    }

    function htmlsp($h){
        return htmlspecialchars($h,ENT_QUOTES,"UTF-8");
    }


?>



<head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

        <title>メンタルメモ</title>
</head>


<body>
         <div class="container">
             <h1>メモ</h1>
             <a href="mentalPage.php">表示画面</a>
             <p>
             <?php echo htmlsp($title.$memo.$move); ?>
             </p>
                <form method="post">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">タイトル</label>
                        <input type="text" class="form-control" name="title" placeholder="タイトル" value="<?php if(!empty($_POST['title'])){echo htmlsp($_POST['title']);} ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">カテゴリー</label>
                        <?php include("selectCategory.php") ?>
                    </div>
                    <!--このdivでカテゴリー入力欄を作る-->
                    <div class="form-goup">
                        <input type="text" class="form-control"  name="newCategory" placeholder="新しくカテゴリーを作成する">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">メモ、記録</label>
                        <textarea class="form-control" rows="3" placeholder="メモ" name="memo"><?php if(!empty($_POST['memo'])){echo htmlsp($_POST['memo']);} ?></textarea>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">登録</button>
                </form>
        </div>
    
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script> 
</body>

