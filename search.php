<?php

    session_start();
    
     $link = mysqli_connect("localhost","root","root","mentalitymemo");
    if(mysqli_connect_error()){
        die("DBへの接続に失敗");
    }

    
    //selectで持ってきたやつを３回まわして表示させる
    $query = "SELECT * FROM `posting` WHERE category = '".mysqli_real_escape_string($link,$_POST['category'])."' ORDER BY id DESC";
    $result = mysqli_query($link,$query);
    $num_rows = mysqli_num_rows($result);
    
    $id = "";
    $title = "";
    $category = "";
    $memo = "";

    function htmlsp($h){
        return htmlspecialchars($h,ENT_QUOTES,"UTF-8");
    }

?>



<head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="overflow.css">

        <title>メンタルメモ</title>
</head>


<body>
        <div class="container">
            <h1>検索結果</h1>
            <a href="mentality.php">メモ用画面へ</a>
            <a href="mentalPage.php">表示画面へ</a>
            
            <div class="col-sm-8">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">カテゴリーで絞る</label>
                    <form method="post">
                        <?php include("selectCategory.php") ?>
                        <button class="btn btn-primary" >絞り込み</button>
                    </form>
                </div>
            </div>
            
            
           <?php //カードの表示
            for($i=0 ; $i < $num_rows ; $i++){
                $row = mysqli_fetch_array($result);
                $id = $row[0];
                $title = $row[1];
                $category = $row[2];
                $memo = $row[3]; ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                    <div class="card-block">
                        <h3 class="card-title">タイトル：<?php echo htmlsp($title); ?></h3>
                        <h5 class="card-subtitle">カテゴリー：<?php echo htmlsp($category); ?></h5>
                        <br>
                        <div class="over"><?php echo nl2br($memo); ?></div>
                        <br>
                        <form action="memoChange.php" method="post">
                            <input type="hidden" name="id" value="<?php echo htmlsp($id); ?>">
                            <button class="btn btn-primary" >本文編集へ</button>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
            <?php ;} ?>
            
            
        </div>
    
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script> 
</body>

