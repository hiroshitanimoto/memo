<?php

    //DBとの連携
    $link = mysqli_connect("localhost","root","root","mentalitymemo");
    //サーバー名(ホスト)、データベースユーザ名、パスワード,データベース名
    if(mysqli_connect_error()){
        die("DBへの接続に失敗しました");
    }

    $querySelect = "SELECT DISTINCT `category` FROM `posting`";
    $resultSelect = mysqli_query($link,$querySelect);

    function h($h){
        return htmlspecialchars($h,ENT_QUOTES,"UTF-8");
    }
?>


<head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

        <title>メモ確認、編集</title>
</head>


<body>
        <div class="container">
            <select class="form-control" name="category">
                <?php
                    for($i=0; $i < mysqli_num_rows($resultSelect); $i++){
                    $rowSelect = mysqli_fetch_array($resultSelect); ?>
                        <option><?php echo h($rowSelect[0]); ?></option>
                <?php } ?>
            </select>      
        </div>
    
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script> 
</body>




