<?php
    $link = mysqli_connect("localhost","root","root","mentalitymemo");
    if(mysqli_connect_error()){
        die("DBへの接続に失敗");
    }

    //selectで持ってきたやつを３回まわして表示させる
    $query = "SELECT * FROM `posting` ORDER BY id DESC";
    $result = mysqli_query($link,$query);
    
  
?>



<head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

        <title>メンタルメモ</title>
</head>


<body>
        <div class="container">
            <h1>メモ表示画面</h1>
            <a href="mentality.php">メモ用画面へ</a>
            
            <div class="form-group">
                <label for="exampleFormControlSelect1">カテゴリー</label>
                <select class="form-control" name="category">
                    <option>勉強</option>
                    <option>意思</option>
                    <option>コミュニケーション</option>
                    <option>筋トレ</option>
                </select>
            </div>
            
          <?php //カードの表示
            for($i=0 ; $i < 3 ; $i++){
                $row = mysqli_fetch_array($result);
                $title = $row[1];
                $category = $row[2];
                $memo = $row[3]; ?>
            <div class="row">
                <div class="col-sm-10">
                    <div class="card">
                    <div class="card-block">
                        <h3 class="card-title"><?php echo $title; ?></h3>
                        <h5 class="card-subtitle"><?php echo $category; ?></h5>
                        <p class="card-text"><?php echo $memo; ?></p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                    </div>
                </div>
            </div>
            <?php ;} ?>
            
            
          
        </div>
    
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script> 
</body>

